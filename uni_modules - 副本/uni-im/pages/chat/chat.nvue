<template>
	<page-meta>
		<navigation-bar :title="conversation.title" background-color="#f8f8f8" front-color="#000000" />
	</page-meta>
	<view class="page">

		<!-- #ifdef H5 -->
		<!-- H5端 左上角显示未读消息数 ，nvue端setTitleNViewButton-->
		<view @click="tapUnreadCount" class="unread_count" v-if="unread_count != 0">
			{{ unread_count > 99 ? '99+' : unread_count }}
		</view>
		<!-- #endif -->

		<uni-im-msg-list v-if="conversation.id" :conversationId="conversation.id" ref="msg-list" @showControl="showControl" @retriesSendMsg="retriesSendMsg" :paddingBottom="imPlaceholderheight + 'px'" class="msg-list"></uni-im-msg-list>

		<!-- 聊天数据输入框 -->
		<view class="chat-foot" :style="{'padding-bottom':keyboardHeight?0:systemInfo.safeAreaInsets.bottom/2+'px'}">
			<!-- pc宽屏（width>960px）的输入框 -->
			<view v-if="isWidescreen" class="pc">
				<view class="tool-bar">
					<view class="icons">
						<uni-im-icons v-for="(item,index) in menuList" :key="index" @click.native.stop="clickMenu(index,$event)" :code="item.iconCode" size="26"></uni-im-icons>
					</view>
					<label class="code-model" style="flex-direction: row;margin-left: 10px;">
						<text>代码模式：</text>
						<switch style="transform: scale(0.8) translateX(-10px);" @change="isCodeText = $event.detail.value"></switch>
					</label>
				</view>
				<view class="answer-msg" v-if="answerMsgIndex !== false">
					<text class="answer-msg-text">你正在回答消息：{{msgList[answerMsgIndex].body}}</text>
					<uni-icons @click="answerMsgIndex = false" type="clear" color="#aaa" size="14px"></uni-icons>
				</view>
				<textarea class="textarea" :maxlength="isCodeText?-1:250" v-model="chatText"></textarea>
			</view>
			<!-- 非pc宽屏（width<960px）的输入框 -->
			<view v-else class="input-box">
				<!-- 切换为语音模式 -->
				<uni-im-icons @click="changeSoundIsShow" :code="soundIsShow?'e69f':'e684'" size="30" class="icon"></uni-im-icons>
				<view class="input-box-conetnt">
					<view class="textarea-box">
						<textarea v-model="chatText" @input="input" @confirm="beforeSendMsg()" @linechange="linechange" :flex="true"
							:style="{ height: textareaHeight + 'px' }" :disable-default-padding="false" :hold-keyboard="true" :confirm-hold="true"
							:auto-blur="false" confirm-type="send" :show-confirm-bar="false" :cursor-spacing="20" maxlength="250"
							:focus="mpIsFocus" @focus="onChatInputFocus()" @blur="isFocus = false" :fixed="true"
							:adjust-position="false" class="textarea" ref="input-box"/>
					</view>
					<view class="answer-msg" v-if="answerMsgIndex !== false">
						<text class="answer-msg-text">你正在回答消息：{{msgList[answerMsgIndex].body}}</text>
						<uni-icons @click="answerMsgIndex = false" type="clear" color="#aaa" size="14px"></uni-icons>
					</view>
					<uni-im-sound class="uni-im-sound" v-if="soundIsShow" @success="sendSound"></uni-im-sound>
				</view>
				<uni-im-icons @click.native.stop="changeEmojiIsShow" :code="emojiIsShow?'e69f':'e646'" size="30" class="icon"></uni-im-icons>
				<text v-if="!soundIsShow&&chatText" @click.prevent="beforeSendMsg()" class="icon beforeSendMsg">发送</text>
				<uni-im-icons v-else @click.native.stop="changeMenuIsShow" code="e75a" size="30" class="icon"></uni-im-icons>
			</view>
			
			<view v-if="menuIsShow||emojiIsShow" class="media-box" :style="{height:keyboardMaxHeight+'px'}">
				<view v-if="menuIsShow" class="menu"  :style="{height:keyboardMaxHeight+'px'}">
					<view class="menu-item" v-for="(item,index) in menuList" :key="index" @click.stop="clickMenu(index,$event)">
						<view class="menu-item-icon">
							<uni-im-icons :code="item.iconCode" size="26"></uni-im-icons>
						</view>
						<text class="menu-item-text">{{item.title}}</text>
					</view>
				</view>
				<scroll-view :scroll-y="true" v-if="emojiIsShow" class="emojiListBox" :style="{height:keyboardMaxHeight+'px'}">
					<text v-for="(uniCodeEmoji,index) in emojiCodes" :key="index" @click.stop="clickEmojiItem(uniCodeEmoji,$event)" 
					class="emoji-item">{{uniCodeEmoji}}</text>
				</scroll-view>
			</view>
			<view v-else :style="{height:keyboardHeight+'px'}"></view>
		</view>
		
		<uni-im-control ref="uni-im-control" @answer="answer"></uni-im-control>
		
		<!-- <uni-icons v-if="hasNewMsg" class="hasNewMsg" @click="showLast(300)" color="#FFF" type="back" :style="{bottom:imPlaceholderheight + 10 + 'px'}"></uni-icons> -->
		
		<!-- <view style="position: fixed;top: 200px;left: 0;background-color: #FFFFFF;">
			imPlaceholderheight:{{imPlaceholderheight}}
			keyboardHeight:{{keyboardHeight}}
			keyboardMaxHeight:{{keyboardMaxHeight}}
			systemInfo.osName:{{systemInfo.osName}}
		</view> -->
	</view>
</template>

<script>
	import {uniImState,uniImMethods,mapUniImState} from '@/uni_modules/uni-im/store/store.js';
	
	// #ifdef APP-NVUE
	// 定义weex的dom模块（https://doc.weex.io/zh/docs/modules/dom.html#scrolltoelement）
	const dom = weex.requireModule('dom') || {};
	// #endif

	import {
		mutations,
		store as uniIdStore
	} from '@/uni_modules/uni-id-pages/common/store';


	import uniImUtils from '@/uni_modules/uni-im/common/utils.js';

	import emojiCodes from '@/uni_modules/uni-im/common/emojiCodes';
	export default {
		data() {
			return {
				// 当前会话对象
				conversation:{
					id:false
				},
				//聊天输入框高度
				textareaHeight: 26,
				//正在加载更多聊天数据
				loadMoreIng: true,
				//收到正在对话的用户发来新消息，时悬浮按钮提示
				hasNewMsg: false,
				hasMore: true,
				//系统信息
				systemInfo: {
					safeAreaInsets:{
						bottom:0
					}
				},
				isCodeText:false,
				menuIsShow:false,
				emojiIsShow:false,
				soundIsShow:false,
				menuList:[
					{
						"title":"图片",
						"iconCode":"e7be"
					},
					{
						"title":"视频",
						"iconCode":"e690"
					},
					{
						"title":"文件",
						"iconCode":"e69e"
					}
				],
				keyboardHeight:0,
				keyboardMaxHeight:260,
				emojiCodes:emojiCodes,
				isFocus:false,
				answerMsgIndex:false
			};
		},
		computed: {
			...mapUniImState(['currentConversationId','conversationDatas','isWidescreen']),
			unread_count(){
				return uniImMethods.conversation.unreadCount()
			},
			isSafariPc() {
				return this.systemInfo.browserName == 'safari' && this.isWidescreen
			},
			msgList() {
				return this.conversation.msgList || []
			},
			//聊天数据
			//当前会话的聊天框文字内容
			chatText: {
				get() {
					// console.log('this.conversation',this.conversation);
					return this.conversation?.chatText;
				},
				set(chatText) {
					this.conversation.chatText = chatText
				}
			},
			//当前用户自己的uid
			current_uid() {
				return uniIdStore.userInfo._id;
			},
			imPlaceholderheight(){
				// #ifdef H5
				//是否为pc宽屏（width>960px）
				if(this.isWidescreen){
					return 0
				}
				// #endif
				let imPlaceholderheight = 32 + this.textareaHeight
				if(this.keyboardHeight || this.menuIsShow || this.emojiIsShow){
					imPlaceholderheight += this.keyboardMaxHeight
				}
				if(! this.keyboardHeight){
					imPlaceholderheight += this.systemInfo.safeAreaInsets.bottom/2
				}
				// console.log('imPlaceholderheight',imPlaceholderheight)
				return imPlaceholderheight
			},
			mpIsFocus(){
				// #ifdef MP
				return this.isFocus
				// #endif
			}
		},
		created() {
			this.systemInfo = uni.getSystemInfoSync()
			// console.log('this.systemInfo',this.systemInfo );
			// #ifdef H5
			//通过监听窗口变化 获取键盘弹出或收起事件
			window.addEventListener('resize', () => {
				if(this.currentConversationId){
					this.showLast(0);
					this.soundIsShow = false
				}
			})
			// #endif
			
			// #ifndef H5
			// 监听键盘高度变化显示最后一条消息
			uni.onKeyboardHeightChange(({
				height
			}) => {
				this.keyboardHeight = height
				console.log('height',height)
				if(height){
					this.keyboardMaxHeight = height
				}
				setTimeout(()=> {
					try{
						this.showLast();
					}catch(e){
						console.log(e)
						//TODO handle the exception
					}
				}, 500);
			});
			// #endif
			// 监听推送消息
			uni.onPushMessage(res => {
				//获取透传内容
				const {
					type,
					data
				} = res.data.payload;
				//判断消息类型是否为im
				if (type == "uni-im" && data.conversation_id == this.currentConversationId) {
					//已经打开的页面收到对应的聊天记录，状态设置为已读
					uniImMethods.clearUnreadCount(this.currentConversationId);
					// console.log('聊天页面-收到消息: ', JSON.stringify(res));
					this.hasNewMsg = true;
					//需要判断滚动条高
					setTimeout(()=> {
						this.showLast();
					}, 1000);
				}
			});
			// #ifdef H5
			//上传图片并发送
			let uploadAndSend = (blobUrl, type,{name,size}) => {
				// console.log('blobUrl',JSON.stringify(blobUrl),);
				uni.showLoading();
				uniCloud.uploadFile({
						filePath: blobUrl,
						cloudPath: 'uni-im/' + this.currentConversationId + Date.now() + (type == 'image'?'.png':'')
					})
					.then( ({fileID:url}) => {
						uni.hideLoading();
						
						let data = {};
						if(!['image','video'].includes(type)){
							type = 'file'
						}
						data[type] = {url,size,name};
						this.beforeSendMsg(data);
					})
					.catch(e=>{
						console.log(e);
					})
			};
			// 以下为实现拖拽或粘贴图片至聊天页面，直接发送的逻辑
			// 阻止默认事件
			document.addEventListener(
				'dragover',
				function(event) {
					event.preventDefault();
				},
				false
			);
			// 拖拽结束时触发
			document.addEventListener(
				'drop',
				e => {
					//取消默认浏览器拖拽效果
					e.preventDefault();
					//获取文件对象
					let fileList = e.dataTransfer.files;
					if (fileList.length == 0) {
						return false;
					}
					let type = 'file'
					if (fileList[0].type.includes('video')) {
						uni.showToast({
							title: 'video',
							icon: 'none'
						});
						type = 'video'
					} else if (fileList[0].type.includes('image')) {
						uni.showToast({
							title: 'image',
							icon: 'none'
						});
						type = 'image'
					}
					let {name,size} = fileList[0]
					// console.log(78979798,fileList);
					let blobUrl = window.URL.createObjectURL(fileList[0]);
					uploadAndSend(blobUrl,type,{name,size});
				},
				false
			);
			// 粘贴时触发
			document.addEventListener('paste', event => {
				if (event.clipboardData || event.originalEvent) {
					//某些chrome版本使用的是event.originalEvent
					let clipboardData = event.clipboardData || event.originalEvent.clipboardData;
					let text = clipboardData.getData('text');
					if (text) {
						// console.log(text);
						if (!this.isCodeText && text.length > 250) {
							uni.showModal({
								content: '你粘贴的文本长度超过250，将被截断。',
								complete: e => {
									if (!e.confirm) {
										setTimeout(() => {
											this.chatText = '';
										}, 10);
									}
								}
							});
						}
					}
					if (clipboardData.items) {
						let items = clipboardData.items,
							len = items.length,
							blob = null;
						for (let i = 0; i < len; i++) {
							// console.log(items[i]);
							if (items[i].type.indexOf('image') !== -1) {
								//getAsFile() 此方法只是living standard firefox ie11 并不支持
								blob = items[i].getAsFile();
							}
						}
						if (blob !== null) {
							let blobUrl = URL.createObjectURL(blob);
							let name = "来源截图工具数据",size = 0
							uni.getFileInfo({
								filePath:blobUrl,
								success:e=>{
									size = e.size
									// console.log(blobUrl,{name,size});
									uploadAndSend(blobUrl, 'image',{name,size});
								}
							})
						}
					}
				}
			});
			// #endif
		},
		mounted() {
			// #ifdef H5
			//实现（shift ctrl alt windows/mac） + enter 是换行输入而不是直接发送
			//定义adjunct建未被按下
			let adjunctKeydown = false;
			//获得消息输入框对象
			const inputBox = document.querySelector('.chat-foot .pc textarea');
			if (inputBox) {
				//键盘按下时
				inputBox.onkeydown = e => {
					// console.log('onkeydown', e.keyCode)
					if ([16, 17, 18, 93].includes(e.keyCode)) {
						//按下了shift ctrl alt windows键
						adjunctKeydown = true;
					}
					// console.log('onkeydown：', e,'adjunctKeydown：', adjunctKeydown);
					// 宽屏 且 按下了enter键 且没按下adjunct键 时直接发送消息
					if (this.isWidescreen && e.keyCode == 13 && !adjunctKeydown) {
						this.beforeSendMsg();
					}
				};
				inputBox.onkeyup = e => {
					//松开adjunct键
					if ([16, 17, 18, 93].includes(e.keyCode)) {
						adjunctKeydown = false;
						// console.log('adjunctKeydown', adjunctKeydown);
					}
				};
			}
			// #endif
		},
		onShow() {
			if (this.conversation.id) {
				// 窗口显示时（因为可能是有其他app切换进已打开的本窗口，所以是onshow而不只是onload）设置全局变量 正在聊天的用户的会话id
				uniImState.currentConversationId = this.conversation.id
				
				let {title,group_id,group_member} = this.conversation
				// console.log('this.conversation',this.conversation);
				if(group_id){
					title += "（"+Object.keys(group_member).length+"人）"
				}
				// console.log('onShow:'+title);
				// uni.setNavigationBarTitle({
				// 	title,
				// 	complete: (e) => {
				// 		// console.log(e);
				// 	}
				// })
			}
		},
		onUnload() {
			// console.log('onUnload');
			//页面销毁之前销毁 全局变量 正在聊天的用户的id
			uniImState.currentConversationId = false
			// console.log('beforeDestroy');
			// 关闭sound播放
			uniImState.audioContext.stop()
		},
		beforeDestroy() {
			//页面销毁之前销毁 全局变量 正在聊天的用户的id
			uniImState.currentConversationId = false
			// console.log('beforeDestroy');
			// 关闭sound播放
			uniImState.audioContext.stop()
		},
		onHide() {
			// 关闭sound播放
			uniImState.audioContext.stop()
		},
		async onLoad(param) {
			//调用load方法，因为pc宽屏时本页面是以组件形式展示。如$refs.chatView.loadconversation_id()执行
			await this.load(param);
		},
		methods: {
			async load(param) {
				this.answerMsgIndex = false
				// conversation_id = "single_eff0518ad35e16a8a025cc8af03e0388"
				
				// console.log('conversation_id',conversation_id);
				this.conversation = await uniImMethods.conversation.get(param)
				// this.conversation.call_list = []
				// console.log('this.conversation',this.conversation)
				
				
				//设置全局的app当前正在聊天的会话id（注：本页面可能是直达页）
				uniImState.currentConversationId = this.conversation.id
				
				setTimeout(()=> {
					this.$refs['msg-list'].init()
				}, 0);
				
				//清除未读数
				if (this.conversation.unread_count) {
					uniImMethods.clearUnreadCount(this.conversation.id);
				}

				//debug用模拟一次性自动发送100条数据
				// for (var i = 0; i < 20; i++) {
				// 	this.chatText = '这是第'+i+'条消息'
				// 	this.beforeSendMsg()
				// }*/
			},
			onChatInputFocus(){
				this.isFocus = true;
				this.$nextTick(()=>{
					this.menuIsShow = false
					this.emojiIsShow = false
				})
			},
			changeSoundIsShow() {
				this.soundIsShow = !this.soundIsShow
				if(this.soundIsShow){
					uni.hideKeyboard();
				}else{
					this.isFocus = true
				}
				uni.$emit('changeSoundIsShow');
				this.$nextTick(()=>{
					this.menuIsShow = false
					this.emojiIsShow = false
				})
			},
			changeMenuIsShow(e){
				if(this.keyboardHeight){
					this.menuIsShow = true
					uni.hideKeyboard()
				}else{
					this.menuIsShow = !this.menuIsShow
				}
				this.emojiIsShow = false;
				this.$nextTick(()=>{
					this.showLast(0)
				})
				e.stopPropagation()
			},
			changeEmojiIsShow(e){
				this.soundIsShow = false
				
				if(this.keyboardHeight){
					this.emojiIsShow = true
					uni.hideKeyboard()
				}else{
					this.emojiIsShow = !this.emojiIsShow
				}
				this.menuIsShow = false
				this.$nextTick(()=>{
					this.showLast(0)
				})
				e.stopPropagation()
			},
			async chooseAndUploadFile(type) {
				uniCloud.chooseAndUploadFile({
					type,
					count: 9,
					sizeType: ['compressed'],
					success: e => {
						// console.log(e);
						e.tempFiles.forEach(event => {
							let {
								url,
								name,
								size
							} = event;
							// console.log('event',event);
							
							// #ifndef APP
							type = event.type.split('/')[0];
							// #endif
							if(!['image','video'].includes(type)){
								type = 'file'
							}
							// console.log('type===>', type);
							let data = {};
							data[type] = {url,size,name};
							this.beforeSendMsg(data)
						});
					},
					fail(e) {
						console.error(e);
						// uni.showToast({
						// 	title: JSON.stringify(e),
						// 	icon: 'none'
						// });
					},
					complete() {
						uni.hideLoading();
					}
				});
			},
			hideKeyboard() {
				// console.log('hideKeyboard');
				uni.hideKeyboard();
				this.$nextTick(()=>{
					this.menuIsShow = false
					this.emojiIsShow = false
					this.isFocus = false
				})
			},
			input() {
				// 此事件仅非宽屏设备才会调用 非 Android端一旦输入换行符合直接发送消息
				if (this.systemInfo.platform != 'android' && this.chatText.indexOf('\n') >= 0) {
					// console.log("有\n",this.chatText, this.chatText.length);
					this.chatText = this.chatText.substring(0, this.chatText.length - 1);
					//检查去除换行符后是否为空
					if (ClearBr(this.chatText).length) {
						this.beforeSendMsg();
					} else {
						uni.showToast({
							title: '不能发送空消息',
							icon: 'none'
						});
						this.chatText = '';
						this.$nextTick(() => {
							this.chatText = '';
							this.textareaHeight = 26;
						});
					}

					function ClearBr(key) {
						key = key.replace(/<\/?.+?>/g, '');
						key = key.replace(/[\r\n]/g, '');
						return key;
					}
				}
			},
			sendSound(e){
				// console.log('sendSound',e);
				this.beforeSendMsg({sound:e})
			},
			async answer(msgIndex){
				this.answerMsgIndex = msgIndex
				console.log('answer',msgIndex)
			},
			async beforeSendMsg(param = {}) {
				// console.log('beforeSendMsg',{param});
				let data = {
					type: 'text',
					to_uid: this.conversation.friend_uid,
					conversation_id: this.conversation.id,
					group_id: this.conversation.group_id,
					client_create_time: Date.now(),
					from_uid: this.current_uid,
					state: 0
				}
				// 根据传入的参数设置消息类型和内容
				for (let key in param) {
					if(param[key]){
						data.type = key
						data.body = param[key]
					}
				}
				// 如果是文本类型需要做一些处理
				if(data.type == 'text'){
					//清除空格
					data.body = this.chatText.trim();
					// console.log('data.body',data.body);
					// 阻止发送空消息
					if(!data.body.length){
						return uni.showToast({
							title: '不能发送空消息',
							icon: 'none'
						});
					}
					// 在下一个事件循环 清除输入框的文本内容
					setTimeout(e => {
						this.chatText = '';
						this.textareaHeight = 26;
					}, 100);
					// 当消息是否为 code类型开启时设置 -- 快速实现，临时方案 后续会优化
					if(this.isCodeText){
						data.type = 'code';
					}
				}
				
				if(this.answerMsgIndex !== false){
					data.about_msg_id = this.msgList[this.answerMsgIndex]._id
				}
				
				// console.log('beforeSendMsg', 3);
				let resMsg = this.conversation.msgList.push(data)
				data.state = 0
				console.log('sendMsg-sendMsg-sendMsg', data);
				await this.conversation.msgManager.localMsg.add(data)
				// console.log('data被localMsg.add引用 会新增一个unique_id',data)
				this.sendMsg(data);
				this.showLast();
			},
			sendMsg(data, callback) {
				// console.log('sendMsg-sendMsg-sendMsg', data);
				const uniImCo = uniCloud.importObject('uni-im-co', {
					customUI: true
				});
				// 接收消息的appId，默认为当前应用的appId。如果你是2个不同appId的应用相互发，请修改此值为相对的appId
				data.appId = this.systemInfo.appId
				let index = this.conversation.msgList.findIndex(i=>i.unique_id == data.unique_id)
				uniImCo.sendMsg(data)
					.then(e => {
						console.log('uniImCo.sendMsg',{e,data});
						data.state = e.errCode === 0 ? 100 : -100;
						data.create_time = e.data.create_time;
						data._id = e.data._id;
						this.conversation.msgList[index] = Object.assign({},data)
						this.conversation.msgManager.localMsg.update(data.unique_id,data)
					})
					.catch(e => {
						uni.showModal({
							content: e.message,
							showCancel: false,
							confirmText: '关闭',
						});
						console.log('uniImCo.sendMsg error:', e.errCode, e.message);
						// 必须要有create_time的值，否则indexDB通过创建时间索引找不到数据
						data.create_time = Date.now();
						data.state = -200;
						this.conversation.msgList.splice(index,1,data)
						this.conversation.msgManager.localMsg.update(data.unique_id,data)
					})
					.finally(e => {
						if (callback) {
							callback(e);
						}
					});
			},
			retriesSendMsg(data) {
				uni.showLoading({
					mask: true
				});
				// console.log('retriesSendMsg', data);
				data.isRetries = true
				this.sendMsg(data, e => {
					uni.hideLoading();
				});
			},
			showLast(duration = 300) {
				let msgListRef = this.$refs['msg-list']
				if(msgListRef){
					msgListRef.showLast(duration)
					this.hasNewMsg = false;
				}
			},
			linechange(e) {
				//console.log(e.detail);
				let {
					height,
					lineCount
				} = e.detail;
				// console.log(height,lineCount);
				if (lineCount === 1) {
					this.textareaHeight = 26;
				} else if (height <= 100) {
					this.textareaHeight = height - 2;
				}
			},
			touchmove(e){
				// console.log('e',e);
			},
			async showControl({
				index,
				msgContentDomInfo,
				coordinate
			}) {
				// console.log('index', index, this.msgList, this.msgList[index]);
				let isSelf = this.msgList[index].from_uid == this.current_uid
				let controlData = {
					msg: this.msgList[index],
					msgIndex: index,
					isInTop: false
				};

				// console.log('msgContentDomInfo', JSON.stringify(msgContentDomInfo));
				let {
					top,
					bottom,
					left,
					right,
					width,
					height
				} = msgContentDomInfo
				controlData.width = width
				if (isSelf) {
					controlData.right = width / 3 + 'px'
					controlData.left = ''
				} else {
					controlData.left = width / 3 + 'px'
					controlData.right = ''
				}

				// #ifdef H5
				if(this.isWidescreen){
					let {left,top} = coordinate
					controlData.right = ''
					controlData.top	 = top - 80 + 'px'
					controlData.left = left - uni.upx2px(200) + 'px'
					
					
					// controlData.bottom = ''
					top = parseInt(controlData.top)
					console.log('toptoptop',top)
					if(top < 150){
						controlData.top = (top + 110 + 'px')
					};
					console.log('toptoptop',top)
				}else{
					if (top < 60) {
						controlData.top = bottom + 55 + 'px'
					} else {
						controlData.top = top - 20 + 'px'
					}
				}
				
				
				
				
				// #endif

				// #ifndef H5
				if (top < 60) {
					controlData.top = bottom + 8 + 'px'
				} else {
					controlData.top = top - 65 + 'px'
				}
				// #endif
				controlData.isInTop = top > 60
				
				this.$refs['uni-im-control'].show(controlData)
				// console.log(index);
			},
			clickMenu(index,e){
				// console.log('clickMenu-',index);
				if(index<2){
					this.chooseAndUploadFile(index === 0 ? 'image' : 'video')
				}
				if(index === 2){
					// #ifdef APP-NVUE
					return uni.showToast({
						title: '暂不支持，发送文件',
						icon: 'none'
					});
					// #endif
					this.chooseAndUploadFile('all')
				}
				e.stopPropagation()
			},
			clickEmojiItem(emojiUniCode,e){
				this.chatText += emojiUniCode
				e.stopPropagation()
			},
			tapUnreadCount() {
				//点击未读消息文字按钮事件
				if (this.isWidescreen) {
					// this.$emit('tapUnreadCount') //点击后会话列表自动滚动 置顶 待读项
					// console.log('tapUnreadCount');
				} else {
					uni.navigateBack();
				}
			},
			getkey(id,index){
				// #ifndef APP
				return index
				// #endif
				// #ifdef APP-NVUE
				return id
				// #endif
			}
		},
		onNavigationBarButtonTap(e) {
			if(e.index === 0){
				if(this.conversation.group_id){
					uni.navigateTo({
						url:"/uni_modules/uni-im/pages/group/info?conversation_id=" + this.conversation.id
					})
				}else{
					// console.log(this.conversation,6565);
					uni.navigateTo({
						url:"/uni_modules/uni-im/pages/chat/info?user_id=" + this.conversation.friend_uid
					})
					// uni.showToast({
					// 	title: '仅群里可见，详细信息',
					// 	icon: 'none'
					// });
				}
			}
			// uni.navigateBack();
		}
	};
</script>

<style lang="scss" scoped>
	/* #ifndef APP-NVUE */
	view {
		display: flex;
		flex-direction: column;
		box-sizing: border-box;
	}

	page {
		overflow-anchor: auto;
		background-color: #efefef;
	}
	/* #endif */
	
	.page{
		/* #ifdef APP-NVUE */
		flex: 1;
		/* #endif */
		
		/* #ifndef APP-NVUE */
		height: calc(100vh - 45px);
		/* #endif */
		
		background-color: #efefef;
		// border: solid 5px #2faf4c;
	}
	
	.chat-foot {
		flex-direction: column;
		border-top: 1rpx solid #BBBBBB ;
		background-color: #F7F7F7;
		width: 750rpx;
		
		position: fixed;
		bottom: 0;
		
		/* #ifndef APP-NVUE */
		z-index: 999;
		// overflow: hidden;
		/* #endif */
	}
	/* #ifdef H5 */
	.pc{
		// .pc内的元素只有pc端打开才显示，样式在index页面
		display: none;
	}
	/* #endif */

	/* #ifdef H5 */
	.chat-foot {
		border: none;
	}
	/* #endif */

	/* #ifndef APP-NVUE */
	.chat-foot * {
		//	border: solid 1px red;
	}
	/* #endif */
	.textarea-box{
		background-color: #ffffff;
		padding: 10px;
		width: 450rpx;
		border-radius: 10px;
	}
	.textarea {
		width: 400rpx;
		padding: 0;
		background-color: #ffffff;
		color: #666666;
		//padding: 20rpx;
		font-size: 32rpx;
	}

	.tip-view {
		position: fixed;
		top: 100px;
		width: 750rpx;
		align-items: center;
		color: #999999;
	}
	
	.tip-null-msg{
		color: #999999;
		font-size: 14px;
	}

	.beforeSendMsg {
		color: #ffffff;
		font-size: 24rpx;
		border-radius: 6px;
		background-color: #2faf4c;
		// width: 80rpx;
		height: 28px;
		line-height: 28px;
		text-align: center;
	}

	.icon {
		width: 70rpx;
		justify-content: center;
		align-items: center;
	}

	.loadMore {
		line-height: 80rpx;
		height: 80rpx;
		text-align: center;
		width: 750rpx;
		color: #adb3b7;
		font-size: 12px;
	}

	.hasNewMsg {
		position: fixed;
		right: 20px;
		background-color: #4CD964;
		border-radius: 100px;
		width: 20px;
		height: 20px;
		line-height: 20px;
		text-align: center;
		transform: rotate(270deg);
	}

	/* #ifndef APP-NVUE */
	.unread_count {
		position: fixed;
		top: 10px;
		left: 70rpx;
		z-index: 9999;
		background-color: #dfe2e9;
		padding: 0 14rpx;
		height: 14px;
		line-height: 14px;
		border-radius: 9px;
		color: #0c1013;
		font-size: 12px;
		margin-top: 3px;
	}
	/* #endif */

	.input-box {
		flex-direction: row;
		padding: 10rpx 18rpx;
		justify-content: space-around;
		align-items: center;
	}
	.menu{
		padding: 36rpx;
		width: 750rpx;
		border-top: solid 1px #ededed;
		flex-direction: row;
		flex-wrap: wrap;
	}
	.menu-item,.menu-item-icon{
		width: 160rpx;
		height: 160rpx;
		justify-content: space-around;
		align-items: center;
	}
	.menu-item-icon{
		width: 80rpx;
		height: 80rpx;
		background-color: #ffffff;
		color: #6F6F6F;
		border-radius: 10px;
	}
	.menu-item-text{
		font-size: 12px;
	}
	.emojiListBox{
		width: 750rpx;
		padding: 27rpx;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: space-between;
	}
	.emoji-item{
		text-align: center;
		font-size: 65rpx;
		width: 87rpx;
		height: 87rpx;
		justify-content: center;
		align-items: center;
		/* #ifndef APP-NVUE */
		display: inline-block;
		/* #endif */
	}
	.uni-im-sound{
		position: absolute;
		top: 0;
		left: 0;
		z-index: 999;
	}
	.system-msg-box{
		width: 750rpx;
		margin-bottom: 10px;
		align-items: center;
	}
	.system-msg{
		background-color: #f2f2f2;
		color: #9d9e9d;
		font-size: 14px;
		height: 30px;
		line-height: 30px;
		padding:0 15rpx;
		border-radius: 8px;
	}
	.answer-msg{
		padding:2px 5px;
		margin:3px 5px;
		margin-top: 5px;
		background-color: #efefef;
		color: #6a6a6a;
		border-radius: 5px;
		flex-direction: row;
		align-items: center;
		justify-content: space-between;
	}
	.answer-msg-text{
		font-size: 12px;
	}
</style>
