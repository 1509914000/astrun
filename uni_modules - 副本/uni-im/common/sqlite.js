let options = {
	name: "uni-im",
	path: '_doc/uni-im.db'
}
export default {
	async init(callback = ()=>{}){
		// await this.clearMsgTable()
		// try{
		// 	await new Promise((resolve, reject) => {
		// 		plus.sqlite.closeDatabase({
		// 			...options,
		// 			success: function(e) {
		// 				console.log(e, 'closeDatabase success!')
		// 				resolve(e)
		// 			},
		// 			fail: function(e) {
		// 				console.error(e, 'closeDatabase failed: ' + JSON.stringify(e))
		// 				reject(e)
		// 			}	
		// 		})
		// 	})
		// }catch(e){
		// 	console.error(e)
		// 	//TODO handle the exception
		// }
	},
	async checkOpenDataBase(){
		let isOpenDatabase = plus.sqlite.isOpenDatabase(options)
		// console.log('isOpenDatabase',isOpenDatabase);
		if (!isOpenDatabase) {
			let res = await new Promise((resolve, reject) => {
				plus.sqlite.openDatabase({
					...options,
					success: function(e) {
						// console.log(e, 'openDatabase success!')
						resolve(e)
					},
					fail: function(e) {
						console.error(e, 'openDatabase failed: ' + JSON.stringify(e))
						reject(e)
					}
				});
			})
			
			let sql = `create table if not exists msg(
					"_id" CHAR(32),
					"body" TEXT,
					"type" CHAR(32),
					"from_uid" CHAR(32),
					"to_uid" CHAR(32),
					"is_read" BLOB,
					"create_time" DATETIME,
					"conversation_id" CHAR(32),
					"group_id" CHAR(32),
					"client_create_time" DATETIME,
					"unique_id" CHAR(32),
					"appid" CHAR(32),
					"state" INT,
					"is_revoke" BLOB,
					"is_delete" BLOB
			)`
			this.executeSql(sql)
			
			return res
		}
	},
	async clearMsgTable(){
		let dd = await this.executeSql('drop table msg')
		console.log('clearMsgTable',dd);
	},
	async executeSql(sql) { //执行executeSql
		await this.checkOpenDataBase()
		// console.log('sql',sql);
		return await new Promise((resolve, reject) => {
			// console.log('执行executeSql');
			plus.sqlite.executeSql({
				name: options.name,
				sql: sql,
				success: function(e) {
					// console.log(e, 'executeSql success!')
					resolve(e)
				},
				fail: function(e) {
					console.error(e,sql)
					console.error(e, 'sql'+sql,'executeSql failed: ' + JSON.stringify(e))
					reject(e)
				}
			});
		})
	},
	async selectSql(sql) { //执行selectSql
		await this.checkOpenDataBase()
		
		return await new Promise((resolve, reject) => {
			// console.log('执行selectSql');
			plus.sqlite.selectSql({
				name: options.name,
				sql: sql,
				success: function(e) {
					// console.log(e, 'selectSql success!')
					resolve(e)
				},
				fail: function(e) {
					console.error(e, 'sql:'+sql,'selectSql failed: ' + JSON.stringify(e))
					reject(e)
				}
			});
		})
	}
}