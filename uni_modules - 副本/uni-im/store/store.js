import MsgManager from '@/uni_modules/uni-im/common/msgManager.js';
import utils from '@/uni_modules/uni-im/common/utils.js'
const db = uniCloud.database();
const uniImCo = uniCloud.importObject("uni-im-co", {
	customUI: true
})
import {
	store as uniIdStore
} from '@/uni_modules/uni-id-pages/common/store'

function current_uid() {
	return uniCloud.getCurrentUserInfo().uid
}
import createObservable from './createObservable';

export const uniImState = createObservable({
	// 会话数据
	conversation: {
		dataList:[],
		hasMore:true,
		loading:false // 加锁防止意外重复请求时出错
	},
	// 正在对话的会话id
	currentConversationId: false,
	// 全局响应式心跳，用于更新消息距离当前时长 等
	heartbeat: '',
	// 好友列表
	friend: {
		dataList:[],
		hasMore:true
	},
	// 群列表
	group: {
		dataList:[],
		hasMore:true
	},
	// 系统通知消息
	systemNotice: {
		dataList:[],
		hasMore:true
	},
	 //存储所有出现过的用户信息，包括群好友信息
	usersInfo: {},
	//是否为pc宽屏
	isWidescreen: false,
	//系统信息
	systemInfo: {},
	// #ifndef H5
	indexDB:false,
	// #endif
	audioContext:false
})

export const uniImMethods = {
	/**
	 * 会话对象
	 * data:会话对象数据模型（conversationDatas是原始数据，data为经过转化的数据）
	 * loadMore：加载更多数据方法
	 */
	conversation:{
		// 消息id 会话id ，操作者id
		async revokeMsg({_id:msg_id,conversation_id},user_id = false){
			console.log({msg_id,conversation_id});
			// userId为操作者id，false表示当前用户操作的。需要广播同步给其他人。收到其他人的撤回指令，会带上触发者的id
			if(!user_id){ // 为空表示当前用户就是操作者
				let res = await uniImCo.revokeMsg(msg_id)
			}
			
			let conversation = await this.get(conversation_id)
			let msgList = conversation.msgList
			let index = msgList.findIndex(item=> item._id == msg_id)
			if(index != -1){
				let msg = msgList[index]
				msg.is_revoke = true
				msg.body = '312123'
				conversation.msgList.splice(index,1,Object.assign({},msg))
				console.log('conversation.msgList',conversation.msgList);
				conversation.msgManager.localMsg.update(msg.unique_id,msg)
			}
		},
		async get(param){
			/**
			 *  字符串	会话id
			 * 	数组		一组会话id（暂不支持）
			 * 	对象类型	会话对方信息（用于如果本地不存在则需要自动创建的场景），包括：{friend_uid,to_uid,from_uid,group_id,user_info}
			 */
			let conversationId = false
			if(param){
				if(typeof param == 'object'){
					let {friend_uid,user_id,group_id,conversation_id} = param
					conversationId = conversation_id
					if(user_id){
						friend_uid = user_id
						param.friend_uid = user_id
					}
					if(!conversationId){
						if(!group_id && !friend_uid){
							console.log('param---------',param);
							throw new Error("会话对象不详，请检查参数",param)
						}
						conversationId = utils.getConversationId(friend_uid || group_id, friend_uid ? 'single':'group')
					}
				}else if(typeof param == 'string'){
					conversationId = param
				}else{
					throw new Error("会话对象不详，请检查参数",param)
				}
			}
			let conversationDatas = uniImState.conversation.dataList
			if(conversationId){
				conversationDatas = conversationDatas.filter(i=>i.id == conversationId)
				if(conversationDatas.length == 0){
					// 本地没有没有就联网查找
					let conversationData = await this.loadMore(conversationId)
					if(conversationData){
						conversationDatas = [conversationData]
					}else{
						if(param.group_id){
							throw new Error("未找到此群会话")
						}
						if(typeof param != 'object'){
							console.log('param',param);
							throw new Error("参数错误")
						}
						// 非群会话，网络也没有就本地创建一个
						if (!param.user_info) {
							let res = await uniCloud.database()
								.collection('uni-id-users')
								.doc(param.friend_uid)
								.field('_id,nickname,avatar_file')
								.get()
							console.log('user_info',res)
							param.user_info = res.result.data[0]
							// console.log('param.user_info', param.user_info);
							if (!param.user_info) {
								throw new Error("用户查找失败")
							}
						}
						let conversationData = {
							group_id:param.group_id,
							friend_uid:param.friend_uid,
							unread_count: 0,
							update_time: Date.now()
						}
						
						try{
							const db = uniCloud.database();
							let res = await db.collection('uni-im-conversation').add(conversationData)
							// console.log('res',res)
						}catch(e){
							throw new Error(e)
						}

						conversationData = Object.assign(conversationData,{
							user_id: current_uid(),
							id: conversationId,
							user_info:param.user_info,
							type:param.friend_uid ? 1:2,
							msgList:[]
						})
						
						// conversationData.msgManager = new MsgManager(conversationData)
						// uniImState.conversation.dataList.push(conversationData)
						this.add(conversationData)
						
						conversationDatas.push(conversationData)
					} 
				}
			}

			// console.log('conversationDatas---',conversationDatas,param);
			// console.log('conversationDatas---',conversationDatas.length,param);
			// 根据会话时间排序
			// console.log('根据会话时间排序')
			conversationDatas = conversationDatas.sort(function(a, b) {
				if(b.id == uniImState.currentConversationId){
					// 当前会话正在输入时，不需要重新排序避免change频繁触发导致排序频繁 
					return 0
				}
				let aml = a.msgList.length
				let bml = b.msgList.length
				let a_update_time = a.update_time
				let b_update_time = b.update_time
				if(aml){
					a_update_time = a.msgList[aml-1].create_time
				}
				if(bml){
					b_update_time = b.msgList[bml-1].create_time
				}
				return b_update_time - a_update_time
			})
			// console.log('999conversationDatas',conversationDatas,conversationId);
			if(conversationId){
				return conversationDatas[0]
			}else{
				// console.log('conversationDatas',conversationDatas)
				return conversationDatas
			}
		},
		/*async bind(key){
			let setValue = async ()=>{
				let data = await uniImMethods.conversation.get()
				console.log('data*--*',data);
				this[key] = data
				return data
			}
			this.$watch(key, async (newValue)=>{
				await setValue()
			})
			return await setValue()
		},*/
		async loadMore(conversation_id){
			// 遗留问题，上一次正在调用，下一次不能马上开始
			// console.log('loadMore-----','loadMore')
			if(!conversation_id){
				if(uniImState.conversation.loading){
					console.log('加载中')
					return []
				}else{
					uniImState.conversation.loading = true
				}
			}
			
			let conversationDatas = uniImState.conversation.dataList
			let lastConversation = conversationDatas[conversationDatas.length - 1]
			let maxUpdateTime = lastConversation ? lastConversation.update_time : ''
			if(conversation_id){
				// 已有会话id的情况下，不设置更新时间条件
				maxUpdateTime = ''
			}
			let res = await uniImCo.getConversationList({
				maxUpdateTime,
				limit: 30,
				conversation_id
			})
			if(res.data.length){
				// console.log('getConversationList res',res);
				this.add(res.data)
			}
			if(!conversation_id){
				uniImState.conversation.loading = false
				uniImState.conversation.hasMore = (res.data.length == 30)
				return res.data
			}else{
				return res.data[0]
			}
		},
		add(data){
			if(!Array.isArray(data)){
				data = [data]
			}
			data.forEach(item=>{
				// 服务端联查到的数据，群和用户信息是数组。这里兼容 客户端add时可直接传object
				if(Array.isArray(item.user_info)){
					item.user_info = item.user_info[0]
				}
				if(Array.isArray(item.group_info)){
					item.group_info = item.group_info[0]
					if(!item.group_member){
						item.group_member = []
					}
				}
				
				// item.chatText = ""
				// item.isInit	= false
				// item.title = ""
				// item.avatar_file = {}
				
				item = Object.assign(item,{
					isInit:false,
					title:"",
					chatText:"",
					avatar_file:{},
					call_list:[]
				})

				if (item.user_info) {
					Object.defineProperties(item,{
						title:{
							get(){
								return item.user_info.nickname
							}
						},
						avatar_file:{
							get(){
								return item.user_info.avatar_file
							}
						},
						group_info:{
							value:false
						}
					})
				} else if (item.group_info) {
					Object.defineProperties(item,{
						title:{
							get(){
								return item.group_info.name
							}
						},
						avatar_file:{
							get(){
								return item.group_info.avatar_file
							}
						},
						user_info:{
							value:false
						}
					})
				}else {
					console.error('会话列表失效，疑似关联用户/群被删除(请改为软删除避免系统异常）：', JSON.stringify(item));
				}
				
				Object.defineProperties(item,{
					last_msg_note:{
						get(){
							let last_msg_note = "暂无记录"
							let last_msg = item.msgList[item.msgList.length - 1]
							// console.log('---last_msg',last_msg)
							if (item.chatText && uniImState.currentConversationId != item.id) {
								last_msg = {
									body: "[uni-im-draft]" + item.chatText,
									type: 'text',
									create_time: Date.now()
								}
							}
							if (last_msg) {
								last_msg_note = '[多媒体]'
								if (last_msg.type == 'text') {
									last_msg_note = last_msg.body.toString()
									last_msg_note = last_msg_note.replace(/[\r\n]/g, "");
									last_msg_note = last_msg_note.slice(0, 30)
								}
								if(last_msg.is_revoke){
									last_msg_note = "消息已被撤回"
								}
								if(last_msg.is_delete){
									last_msg_note = "消息已被删除"
								}
							}
							return last_msg_note
						}
					},
					// update_time:{
					// 	get() {
					// 		let last_msg = item.msgList[item.msgList.length - 1]
					// 		if(last_msg){
					// 			return last_msg.create_time
					// 		}else{
					// 			return item.update_time
					// 		}
					// 	}
					// }
				})
				
				// 把user_info统一存到一个对象
				let {user_info,group_member} = item
				let usersInfo = {}
				if(user_info){
					usersInfo[user_info._id] = user_info
				}else if(group_member){
					usersInfo = group_member.reduce((sum, current) => {
						sum[current.user_id] = current.user_info[0]
						return sum
					}, {})
					item.group_member = usersInfo
				}
				uniImMethods.mergeUsersInfo(usersInfo)
				item.msgManager = new MsgManager(item)
				// console.log('uniImState.conversation.dataList',uniImState.conversation.dataList);
				uniImState.conversation.dataList.push(item)
			})
			
			// console.log('存到storage',uniImState.conversation)
			// 存到storage
			uni.setStorage({
				key:'uni-im-conversation' + '_uid:' + current_uid(),
				data:uniImState.conversation
			})
			return data
		},
		// 统计所有消息的未读数
		unreadCount() {
			let conversationDatas = uniImState.conversation.dataList
			return conversationDatas.reduce((sum, item, index, array) => sum + item.unread_count, 0)
		},
		remove(id){
			let index = uniImState.conversation.dataList.findIndex(i=>i.id==id)
			uniImState.conversation.dataList.splice(index,1)
		}
	},
	/**
	 * 系统消息
	 */
	systemNotice:{
		get:({type,excludeType} = {}) => {
			const systemNoticeDatas = uniImState.systemNotice.dataList
			if(systemNoticeDatas){
				return systemNoticeDatas.reduce((sum, item) => {
					if (type) {
						//兼容字符串和数组
						typeof type == 'string' ? type = [type] : ''
						if (type.includes(item.payload.subType)) {
							sum.push(item)
						}
					} else if (excludeType) {
						//兼容字符串和数组
						typeof excludeType == 'string' ? excludeType = [excludeType] : ''
						if (!excludeType.includes(item.payload.subType)) {
							sum.push(item)
						}
					} else {
						sum.push(item)
					}
					return sum
				}, [])
			}else{
				return false
			}
		},
		loadMore:async ()=>{
			let res = await db.collection('uni-push-message')
				.aggregate()
				.match('"payload.type" == "SystemNotice" && "user_id" == $cloudEnv_uid')
				.sort({
					create_time: -1
				})
				.limit(1000)
				.end()
			let systemNoticeDatas = res.result.data.reverse()
			
			// console.log('systemNoticeDatas',systemNoticeDatas);
			
			let obj = {}
			for (var i = 0; i < systemNoticeDatas.length; i++) {
				let item = systemNoticeDatas[i]
				// 去重操作
				let {subType,unique} = item.payload
				obj[unique ? (subType + "_" + unique) : (Date.now() + "_" + i)] = item
			}
			let data = []
			for (let key in obj) {
				let item = obj[key]
				data.push(item)
			}
			uniImState.systemNotice.dataList.push(...data)
		},
		unreadCount(param = {}){
			let systemNoticeDatas = this.get(param)
			let unreadCount = systemNoticeDatas.reduce((sum, item, index, array) => {
				if (!item.is_read) {
					sum++
				}
				return sum
			}, 0)
			
			// console.log('最新的未读数:', unreadCount, data);
			// 注意：在非tabbar页面无法设置 badge
			try {
				if (unreadCount === 0) {
					uni.removeTabBarBadge({
						index: 2,
						complete: (e) => {
							// console.log(e)
						}
					})
				} else {
					uni.setTabBarBadge({
						index: 2,
						text: unreadCount + '',
						complete: (e) => {
							// console.log(e)
						}
					})
				}
			} catch (e) {
				console.error(e)
			}
			if (unreadCount) {
				return unreadCount + ''
			} else {
				return ''
			}
		}
	},
	friend:{
		get(){
			return uniImState.friend.dataList
		},
		async loadMore({friend_uid} = {}){
			let whereString = '"user_id" == $cloudEnv_uid'
			if (friend_uid) {
				whereString += `&& "friend_uid" == "${friend_uid}"`
				// console.log('whereString',whereString);
			}
			let res = await db.collection(
				db.collection('uni-im-friend').where(whereString).field('friend_uid,mark,class_name').getTemp(),
				db.collection('uni-id-users').field('_id,nickname,avatar_file').getTemp()
			)
			.limit(500)
			.get()
			let data = res.result.data
			// console.log('data',data);
			data.forEach((item, index) => {
				data[index] = item.friend_uid[0]
				let uid = data[index]._id
				if (!uniImState.usersInfo[uid]) {
					uniImState.usersInfo[uid] = item.friend_uid[0]
				}
			})
			uniImState.friend.hasMore = data.length == 500
			uniImState.friend.dataList.push(...data)
		},
		remove(friend_uid){
			let friendList = uniImState.friend.dataList
			let index = friendList.findIndex(i=>i._id==friend_uid)
			friendList.splice(index,1)
		}
	},
	group:{
		get(){
			return uniImState.group.dataList
		},
		async loadMore({group_id} = {}){
			let whereString = '"user_id" == $cloudEnv_uid'
			if (group_id) {
				whereString += `&& "group_id" == "${group_id}"`
				// console.log('whereString',whereString);
			}
			
			let res = await db.collection(
				db.collection('uni-im-group-member')
				.where(whereString)
				.getTemp(),
				db.collection('uni-im-group').getTemp()
			)
			.limit(500)
			.get()
			
			res.result.data.map(item => {
				item.group_info = item.group_id[0]
				delete item.group_id
				return item
			})
			uniImState.group.hasMore = res.result.data.length == 500
			uniImState.group.dataList = res.result.data
		},
		remove({group_id}){
			let groupList = uniImState.group.dataList
			let index = groupList.findIndex(i=>i.id == group_id)
			if(index != -1){
				groupList.splice(index,1,data)
			}
		},
	},
	mergeUsersInfo(usersInfo) {
		uniImState.usersInfo = Object.assign({}, uniImState.usersInfo, usersInfo)
	},
	async clearUnreadCount( conversation_id) {
		let conversation = await this.conversation.get(conversation_id)
		conversation.unread_count = 0
		// 触发器会触发消息表的 is_read = true
		uniCloud.database()
			.collection('uni-im-conversation')
			.where({
				user_id: current_uid(),
				id: conversation_id
			})
			.update({
				"unread_count": 0
			}).then(e => {
				console.log('设置为已读', e.result.updated);
			})
	}
}

export const mapUniImState = function(keys = []){
	let obj = {}
	keys.forEach((key)=>{
		let keyName = key,keyCName = false
		if(key.includes(' as ')){
			let _key = key.trim().split(' as ')
			keyName = _key[0]
			keyCName = _key[1]
		}
		obj[keyCName || keyName] = function(){
			return uniImState[keyName]
		}
	})
	return obj
}

// export const uniIm = Object.assign(uniImState,uniImMethods)