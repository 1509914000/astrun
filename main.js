import App from './App'
// #ifndef VUE3
import Vue from 'vue'
import store from '@/store';
import htmlToPdf from './const/htmlToPdf.js'


let vuexStore = require("@/store/$u.mixin.js");
Vue.mixin(vuexStore);
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
Vue.use(htmlToPdf)
Vue.config.productionTip = false
App.mpType = 'app'



import mixin from "./utils/mixin.js"
Vue.use(mixin)

import navbar from './components/navbar/index.vue'
Vue.component('navbar', navbar)


// 关闭生产提示
Vue.config.productionTip = false;
// Vue.prototype.$store = store;
// 小程序页面组件和这个`App.vue`组件的写法和引入方式是一致的，为了区分两者，需要设置`mpType`值
App.mpType = 'app';
// import {
// 	mapState,
// 	mapMutations
// } from 'vuex';



const app = new Vue({
	...App
})

require('@/utils/request.js')(app)

Vue.prototype.goByPath = function(path, params, skipType = 'navigate', animationType = 'pop-in', animationDuration =
	300) {
	let token = uni.getStorageSync('xtoken')
	if (!token) {
		uni.showToast({
			title:'请登录',
			icon: 'none'
		})
		setTimeout(() => {
			uni.reLaunch({
				url: '/pages/login'
			})
		},1000)
		return
	}
	if (skipType == 'navigate') {
		uni.navigateTo({
			url: path + "?params=" + encodeURIComponent(JSON.stringify(params)),
			animationType: animationType,
			animationDuration: animationDuration
		})
	} else if (skipType == 'switchTab') {
		uni.switchTab({
			url: path
		})
	} else {
		uni.redirectTo({
			url: path + "?params=" + encodeURIComponent(JSON.stringify(params)),
			animationType: animationType,
			animationDuration: animationDuration
		})
	}
}
Vue.prototype.goBack = function() {
	uni.navigateBack({
		delta: 1
	});
}

Vue.prototype.$toast = function(msg) {
	uni.showToast({
		title: msg,
		icon: 'none'
	})
}


app.$mount()
// #endif

