// 此vm参数为页面的实例，可以通过它引用vuex中的变量
import urlList from './config'
module.exports = (vm) => {
	// 初始化请求配置
	
	// 第一次登录随机设置一条线路
	let network_active = uni.getStorageSync('network_active') || Math.floor(Math.random() * urlList.length)
	uni.setStorageSync('network_active',network_active)
	
	uni.$u.http.setConfig((config) => {
		config.baseURL = uni.getStorageInfo('domain') || '/'
		uni.request({
			url: urlList[network_active].url,
			success: (res) => {
				uni.setStorageSync('domain', res.data.http_url)
				config.baseURL =  res.data.http_url /* 根域名 */
			}
		});
		config.custom = {
			auth: false
		}
		config.header = {
			'content-type': 'application/x-www-form-urlencoded',
		}
		return config
	})

	// 请求拦截
	uni.$u.http.interceptors.request.use((config) => { // 可使用async await 做异步操作
		config.data = config.data || {}
		
		if (!config.baseURL) {
			uni.request({
				url: urlList[network_active].url,
				success: (res) => {
					config.baseURL =  res.data.http_url
					let xtoken = uni.getStorageSync("xtoken")
					if(xtoken){
						config.header.authorization = xtoken
					}
					return config
				}
			})
		} else {
			// 根据custom参数中配置的是否需要token，添加对应的请求头
			let xtoken = uni.getStorageSync("xtoken")
			if(xtoken){
				config.header.authorization = xtoken
			}
			return config
		}
	}, config => { // 可使用async await 做异步操作
		uni.showModal({
			content: JSON.stringify(config)
		})
		return Promise.reject(config)
	})

	// 响应拦截
	uni.$u.http.interceptors.response.use((response) => {
		/* 对响应成功做点什么 可使用async await 做异步操作*/
		let data = response.data

		if (data.code<0) {
			uni.showToast({
				title: data.msg,
				icon: 'none'
			})
			if(data.code < -1000  && data.code > -1010 ){
				uni.redirectTo({
					url:'/pages/login'
				})
				return
			}
			uni.redirectTo({
				url:'/pages/login'
			})
			
		}
		return data 
	}, (response) => {
		// 对响应错误做点什么 （statusCode !== 200）
		return Promise.reject(response)
	})
}
