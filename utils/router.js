export default function router(url, mode='navigateTo') {
	return new Promise((res, rej) => {	
		if (mode != false) {
			let token = uni.getStorageSync('xtoken')
			if (!token) {
				uni.reLaunch({
					url: '/pages/login',
					success: () => {
						res()
					},
					fail: () => {
						rej()
					}
				})
				return
			}
		}
	
		uni[mode]({
			url,
			// animationType: 'pop-in',
			// animationDuration: 200,
			animation:{
				mode: "",
		    durationIn: 300,
	      durationOut: 300
			},
			success: () => {
				res()
			},
			fail: () => {
				rej()
			}
		})
	})
}