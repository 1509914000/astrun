import router from './router'
// import {imgUrl} from './config'

export default {
	install(Vue) {
		Vue.mixin({
			data() {
				return {
					paddingTop: 0,
				}
			},
			onLoad() {
				uni.getSystemInfo({
					success: (e) => {
						this.paddingTop = e.statusBarHeight + uni.upx2px(80) + 'px'
					}
				})
			},
			methods: {
				$baseUrl: function() {
					return uni.getStorageSync('domain') + '/'
				},
				$imgUrl: function(src) {
					return uni.getStorageSync('domain') + src
				},
				$jump: router,
				$back: function() {
					uni.navigateBack({
						delta: 1,
						animation:{
			        mode: "",
				      durationIn: 300,
						  durationOut: 300
						},
					})
				},
				
			}
		})
	}
}
